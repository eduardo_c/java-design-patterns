package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public abstract class AbstractFactory {

    abstract IColor getColor(String color);
    abstract IShape getShape(String Shape);
}
