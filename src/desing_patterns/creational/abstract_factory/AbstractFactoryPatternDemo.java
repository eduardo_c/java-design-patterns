package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class AbstractFactoryPatternDemo {

    public static void main(String[] args){
        //get shape factory
        ShapeFactory shapeFactory = (ShapeFactory) FactoryProducer.getFactory("shape");

        //get a Circle object
        IShape shape1 = shapeFactory.getShape("circle");
        shape1.draw();

        //get a Rectangle object
        IShape shape2 = shapeFactory.getShape("rectangle");
        shape2.draw();

        //get a color factory
        ColorFactory colorFactory = (ColorFactory) FactoryProducer.getFactory("color");

        //get a Red object
        IColor color1 = colorFactory.getColor("red");
        color1.fill();

        //get a Green object
        IColor color2 = colorFactory.getColor("green");
        color2.fill();
    }
}
