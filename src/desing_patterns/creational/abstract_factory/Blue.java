package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class Blue implements IColor {
    @Override
    public void fill() {
        System.out.println("Inside Blue::fill() method.");
    }
}
