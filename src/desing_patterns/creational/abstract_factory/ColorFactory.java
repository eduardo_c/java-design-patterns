package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class ColorFactory extends AbstractFactory {
    @Override
    IColor getColor(String color) {
        if(color == null)
            return null;

        if(color.equalsIgnoreCase("RED"))
            return new Red();
        else if(color.equalsIgnoreCase("GREEN"))
            return new Green();
        else if(color.equalsIgnoreCase("BLUE"))
            return new Blue();

        return null;
    }

    @Override
    IShape getShape(String Shape) {
        return null;
    }
}
