package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class FactoryProducer {

    public static AbstractFactory getFactory(String type){
        if(type.equalsIgnoreCase("SHAPE"))
            return new ShapeFactory();
        else if(type.equalsIgnoreCase("COLOR"))
            return new ColorFactory();

        return null;
    }
}
