package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public interface IColor {
    void fill();
}
