package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class Red implements IColor {
    @Override
    public void fill() {
        System.out.println("Inside Red::fill() method.");
    }
}
