package desing_patterns.creational.abstract_factory;

/**
 * Created by jcastro on 16/12/2016.
 */
public class ShapeFactory extends AbstractFactory{
    @Override
    IColor getColor(String color) {
        return null;
    }

    @Override
    IShape getShape(String shapeType) {
        if(shapeType == null)
            return null;
        if(shapeType.equalsIgnoreCase("CIRCLE"))
            return new Circle();
        else if(shapeType.equalsIgnoreCase("RECTANGLE"))
            return new Rectangle();
        else if(shapeType.equalsIgnoreCase("SQUARE"))
            return new Square();

        return null;
    }
}
