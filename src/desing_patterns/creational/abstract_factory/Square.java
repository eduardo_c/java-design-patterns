package desing_patterns.creational.abstract_factory;

/**
 * Created by eduardo on 04/12/2016.
 */
public class Square implements IShape {
    @Override
    public void draw() {
        System.out.println("Inside sqaure::draw() method.");
    }
}
