package desing_patterns.creational.builder;

/**
 * Created by jcastro on 06/01/2017.
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "bottle";
    }
}
