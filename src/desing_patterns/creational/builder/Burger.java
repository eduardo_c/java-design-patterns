package desing_patterns.creational.builder;

/**
 * Created by jcastro on 06/01/2017.
 */
public abstract class Burger implements Item {
    @Override
    public Packing packing(){
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
