package desing_patterns.creational.builder;

/**
 * Created by jcastro on 03/02/2017.
 */
public class ChickenBurger extends Burger {

    @Override
    public String name() {
        return "Chicken Burger";
    }

    @Override
    public float price() {
        return 50.0f;
    }
}
