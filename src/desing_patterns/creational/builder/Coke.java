package desing_patterns.creational.builder;

/**
 * Created by jcastro on 03/02/2017.
 */
public class Coke extends ColdDrink {
    @Override
    public String name() {
        return "Coke";
    }

    @Override
    public float price() {
        return 30.0f;
    }
}
