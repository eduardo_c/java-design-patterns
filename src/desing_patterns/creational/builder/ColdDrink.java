package desing_patterns.creational.builder;

/**
 * Created by jcastro on 06/01/2017.
 */
public abstract class ColdDrink implements Item {
    @Override
    public Packing packing(){
        return new Bottle();
    }

    @Override
    public abstract float price();
}
