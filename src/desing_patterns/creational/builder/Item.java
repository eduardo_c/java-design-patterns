package desing_patterns.creational.builder;

/**
 * Created by jcastro on 19/12/2016.
 */
public interface Item {

    public String name();
    public Packing packing();
    public float price();
}
