package desing_patterns.creational.builder;

/**
 * Created by jcastro on 03/02/2017.
 */
public class Pepsi extends ColdDrink {
    @Override
    public String name() {
        return "Pepsi";
    }

    @Override
    public float price() {
        return 35.0f;
    }
}
