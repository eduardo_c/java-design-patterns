package desing_patterns.creational.builder;

/**
 * Created by jcastro on 06/01/2017.
 */
public class VegBurger extends Burger {
    @Override
    public String name() {
        return "Veg Burger";
    }

    @Override
    public float price() {
        return 25.0f;
    }
}
