package desing_patterns.creational.builder;

/**
 * Created by jcastro on 19/12/2016.
 */
public class Wrapper implements Packing{
    @Override
    public String pack() {
        return "wrapper";
    }
}
