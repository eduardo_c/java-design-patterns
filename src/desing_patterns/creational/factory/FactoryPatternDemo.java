package desing_patterns.creational.factory;

/**
 * Created by eduardo on 04/12/2016.
 */
public class FactoryPatternDemo {
    public static void main(String[] args){
        ShapeFactory shapeFactory = new ShapeFactory();

        IShape shape1 = shapeFactory.getShape("CIRCLE");
        shape1.draw();

        IShape shape2 = shapeFactory.getShape("RECTANGLE");
        shape2.draw();

        IShape shape3 = shapeFactory.getShape("SQUARE");
        shape3.draw();
    }
}
