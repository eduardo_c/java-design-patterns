package desing_patterns.creational.factory;

/**
 * Created by eduardo on 04/12/2016.
 */
public interface IShape {
    void draw();
}
