package desing_patterns.creational.factory;

/**
 * Created by eduardo on 04/12/2016.
 */
public class Rectangle implements IShape {
    @Override
    public void draw() {
        System.out.println("Inside rectangle::draw() method.");
    }
}
