package desing_patterns.creational.factory;

/**
 * Created by eduardo on 04/12/2016.
 */
public class ShapeFactory {

    public IShape getShape(String shapeType){
        if(shapeType == null)
            return null;
        if(shapeType.equalsIgnoreCase("CIRCLE"))
            return new Circle();
        else if(shapeType.equalsIgnoreCase("RECTANGLE"))
            return new Rectangle();
        else if(shapeType.equalsIgnoreCase("SQUARE"))
            return new Square();

        return null;
    }
}
