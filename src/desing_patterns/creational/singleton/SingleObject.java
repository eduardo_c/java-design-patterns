package desing_patterns.creational.singleton;

/**
 * Created by jcastro on 19/12/2016.
 */
public class SingleObject {

    private static SingleObject instance = new SingleObject();

    private SingleObject(){

    }

    public static SingleObject getInstance(){
        return instance;
    }

    public void showMessage(){
        System.out.println("Hola mundo!");
    }
}
