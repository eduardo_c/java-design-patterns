package desing_patterns.creational.singleton;

/**
 * Created by jcastro on 19/12/2016.
 */
public class SingletonPatternDemo {

    public static void main(String[] args){
        SingleObject object = SingleObject.getInstance();

        object.showMessage();
    }
}
